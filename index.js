const open = require('open');
(async () => {
    // Opens the url in the default browser
    await open('http://localhost:3000');

})();


//app configuration
let express = require('express');
let app = express();
app.use(express.static('static'));

//template engine
let nunjucks = require('nunjucks');
nunjucks.configure('views', {
    autoescape: true,
    express: app
});


//db connection
let knex = require('knex')({
  client: 'sqlite3',
  connection: {
    filename: "./subset.db"
  }
});

let _ = require('lodash');

function toTimestamp(strDate){
    let A = _.toInteger(strDate.slice(0,4));
    let M = _.toInteger(strDate.slice(5,7))-1;
    let D = _.toInteger(strDate.slice(8,10));
    let H = _.toInteger(strDate.slice(11,13));
    return Date.UTC(A,M,D,H);

}

app.get('/api/pie', (req, res) => {
    knex.column('timestamp', 'carType').groupBy('carType').count('carType').select().from('dataset').then(function(data){ 
      let pieData = [];
      let ts = req.params.timestamp;
      let color_list = {'2 axle car': '#7cb5ec', '2 axle truck': '#434348', '3 axle truck': '#90ed7d', '4 axle+ truck': '#f7a35c', '2 axle bus': '#8085e9', '3 axle bus': '#f15c80', 'park patrol': '#e4d354'};
      for (let i = 0; i < data.length; i++) {
        pieData.push({
          name: data[i]['carType'],
          y: data[i]["count(`carType`)"],
          color: color_list[data[i]['carType']]
        });
      }
      res.json(pieData);
    });
});

app.get('/api/timechart/:cartype', (req, res) => {
    knex.column('timestamp', 'carType').groupBy('timestamp').groupBy('carType').count('carType').select().from('dataset').then(function(data){ 
      let ct = req.params.cartype;
      let appoggio = [
        {type:'column', name:'2 axle car', data: [], color: '#7cb5ec'}, 
        {type:'column', name:'2 axle truck', data: [], color: '#434348'}, 
        {type:'column', name:'3 axle truck', data: [], color: '#90ed7d'}, 
        {type:'column', name:'4 axle+ truck', data: [], color: '#f7a35c'}, 
        {type:'column', name:'2 axle bus', data: [], color: '#8085e9'}, 
        {type:'column', name:'3 axle bus', data: [], color: '#f15c80'}, 
        {type:'column', name:'park patrol', data: [], color: '#e4d354'}
      ];


      for (var i = 0; i < appoggio.length; i++) {
        for (var j = 0; j < data.length; j++) {
          if (ct == 'default') {
            if (appoggio[i]['name'] == data[j]['carType']) {
              appoggio[i]['data'].push([
                toTimestamp(data[j]["timestamp"]),
                data[j]["count(`carType`)"]
              ]);
            }
          } else {
            if (appoggio[i]['name'] == data[j]['carType'] && ct == data[j]['carType']) {
              appoggio[i]['data'].push([
                toTimestamp(data[j]["timestamp"]),
                data[j]["count(`carType`)"]
              ]);
            }
          }
        }
      }
      res.json(appoggio);
   });
});

app.get('/api/gatechart/:cartype', (req, res) => {
    knex.column('timestamp', 'gateName', 'carType').groupBy('timestamp').groupBy('gateName').count('gateName').select().from('dataset').then(function(data){ 
      let ct = req.params.cartype;
      let appoggio = [
        {type:'column', name:'entrance0', data: []},
        {type:'column', name:'entrance1', data: []},
        {type:'column', name:'entrance2', data: []},
        {type:'column', name:'entrance3', data: []},
        {type:'column', name:'entrance4', data: []},
        {type:'column', name:'ranger-stop0', data: []}, 
        {type:'column', name:'ranger-stop2', data: []}
      ];


      for (var i = 0; i < appoggio.length; i++) {
        for (var j = 0; j < data.length; j++) {
          if (ct == 'default') {
            if (appoggio[i]['name'] == data[j]['gateName']) {
              appoggio[i]['data'].push([
                toTimestamp(data[j]["timestamp"]),
                data[j]["count(`gateName`)"]
              ]);
            }
          } else {
            if (appoggio[i]['name'] == data[j]['gateName']  && ct == data[j]['carType']) {
              appoggio[i]['data'].push([
                toTimestamp(data[j]["timestamp"]),
                data[j]["count(`gateName`)"]
              ]);
            }
          }
        }
      }
      res.json(appoggio);
   });
});

app.get('/api/campinggatechart/:cartype', (req, res) => {
    knex.column('timestamp', 'gateName', 'carType').groupBy('timestamp').groupBy('gateName').count('gateName').select().from('dataset').then(function(data){ 
      let ct = req.params.cartype;
      let appoggio = [
        {type:'column', name:'camping0', data: []},
        {type:'column', name:'camping1', data: []},
        {type:'column', name:'camping2', data: []},
        {type:'column', name:'camping3', data: []},
        {type:'column', name:'camping4', data: []},
        {type:'column', name:'camping5', data: []}, 
        {type:'column', name:'camping6', data: []},
        {type:'column', name:'camping7', data: []},
        {type:'column', name:'camping8', data: []}
      ];


      for (var i = 0; i < appoggio.length; i++) {
        for (var j = 0; j < data.length; j++) {
          if (ct == 'default') {
            if (appoggio[i]['name'] == data[j]['gateName']) {
              appoggio[i]['data'].push([
                toTimestamp(data[j]["timestamp"]),
                data[j]["count(`gateName`)"]
              ]);
            }
          } else {
            if (appoggio[i]['name'] == data[j]['gateName']  && ct == data[j]['carType']) {
              appoggio[i]['data'].push([
                toTimestamp(data[j]["timestamp"]),
                data[j]["count(`gateName`)"]
              ]);
            }
          }
        }
      }
      res.json(appoggio);
   });
});

app.get('/api/rangergatechart/:cartype', (req, res) => {
    knex.column('timestamp', 'gateName', 'carType').groupBy('timestamp').groupBy('gateName').count('gateName').select().from('dataset').then(function(data){ 
      let ct = req.params.cartype;
      let appoggio = [
        {type:'column', name:'ranger-base', data: []},
        {type:'column', name:'ranger-stop1', data: []}, 
        {type:'column', name:'ranger-stop3', data: []},
        {type:'column', name:'ranger-stop4', data: []},
        {type:'column', name:'ranger-stop5', data: []},
        {type:'column', name:'ranger-stop6', data: []},
        {type:'column', name:'ranger-stop7', data: []}
      ];


      for (var i = 0; i < appoggio.length; i++) {
        for (var j = 0; j < data.length; j++) {
          if (ct == 'default') {
            if (appoggio[i]['name'] == data[j]['gateName']) {
              appoggio[i]['data'].push([
                toTimestamp(data[j]["timestamp"]),
                data[j]["count(`gateName`)"]
              ]);
            }
          } else {
            if (appoggio[i]['name'] == data[j]['gateName']  && ct == data[j]['carType']) {
              appoggio[i]['data'].push([
                toTimestamp(data[j]["timestamp"]),
                data[j]["count(`gateName`)"]
              ]);
            }
          }
        }
      }
      res.json(appoggio);
   });
});

app.get('/api/forcechart/', (req, res) => {
  
});


//routing
app.get('/', function(req, res){
  res.render('index.html')
});

app.get('/force', function(req, res){
  res.render('force.html')
});

//app listener
app.listen(3000, function () {
    console.log('App listening on port 3000!');
});